#pragma once
class Therapist
{
public:
	Therapist();
	~Therapist();
	
	void TherapistTalk();
private:
};

class Doctor
{
public:
	Doctor();
	~Doctor();

	void DoctorTalk();
private:
};

class Priest
{
public:
	Priest();
	~Priest();

private:
	
};
