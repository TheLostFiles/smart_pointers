#include "Crazy.h"
#include <iostream>
#include <memory>
#include <memory>



void main()
{
	std::cout << "You are in a padded room that is a box and another larger box around that.\n\n";
	
	std::cout << "You can hear someone come into the outer room\n\n";
	{
		std::unique_ptr<Therapist> therapist = std::make_unique <Therapist>();
		therapist->TherapistTalk();
	}
	std::cout << "\nHe Left the outer room\n";
	std::cout << std::endl;

	system("pause"); system("CLS");
	
	std::cout << "You can hear someone come into the outer room\n\n";
	{
		std::shared_ptr<Doctor> doc;
		std::cout << "They come into the inner room\n\n";
		{
			std::shared_ptr<Doctor> doc2 = std::make_shared<Doctor>();
			doc = doc2;
			doc2->DoctorTalk();
		}
		std::cout << "\nShe Left the inner room\n\n";
	}
	std::cout << "\nShe Left the outer room\n\n";

	system("pause"); system("CLS");

	std::cout << "You can hear someone come into the outer room\n\n";
	{
		std::weak_ptr<Priest> weakPriest1;
		std::cout << "They come into the inner room\n\n";
		{
			std::weak_ptr<Priest> sharedPriest2 = std::make_shared<Priest>();
			weakPriest1 = sharedPriest2;
		}
		std::cout << "\nThrough your in and outs of consciousness, you hear him close your inner doors\n\n";
	}
	std::cout << "Lastly you hear him leave the outer doors.\n\n";

	
	std::cout << "Thank you for coming to this short story\nPress enter to exit";
	std::cin.get();
}
